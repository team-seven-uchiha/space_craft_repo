var ControlsScript = document.createElement("script")
ControlsScript.setAttribute("src", "./js/Controls.js")
document.head.appendChild(ControlsScript)

var MainScript = document.createElement("script")
MainScript.setAttribute("src", "./js/scene.js")
document.head.appendChild(MainScript)

var ActiveKeys = {
    Up    : false,
    Right : false,
    Down  : false,
    Left  : false,
    shoot : false
}

var Player
var Game = {
    Score : 0,
    Deff_kamikazes : 2000,
    Deff_Shooters  : 5000
}
var Enemies = {
    kamikazes: [],
    Shooters: []
}


function PosOrNeg() { return Math.random() < 0.5 ? -1 : 1 }

function RandVal(limit) {
    return Math.random() * limit * PosOrNeg()
}

function CreateEnemie(Type) {
    var Tmp = new Model(Type)
    Tmp.live(RandVal(45), 0, -20)
    return Tmp
}

function CreateKamikazes() {
    Game.Deff_kamikazes = ( Game.Score % 50 == 0 && Game.Deff_kamikazes >= 500) ? Game.Deff_kamikazes - 200 : Game.Deff_kamikazes
    var Tmp = CreateEnemie(ObjectType.Enm_2)
    Enemies.kamikazes.push(Tmp)
    Tmp.AutoMoveDown()
    if(Player != null || Player != undefined) setTimeout(CreateKamikazes, Game.Deff_kamikazes);
}


function CreateShooter() {
    Game.Deff_Shooters = ( Game.Score % 100 == 0 && Game.Deff_Shooters >= 2000) ? Game.Deff_Shooters - 1000 : Game.Deff_Shooters
    var Tmp = CreateEnemie(ObjectType.Enm_1)
    Enemies.Shooters.push(Tmp)
    Tmp.MoveHorizontal(100)
    Tmp.AutoShoot(1000)
    if(Player != null || Player != undefined) setTimeout(CreateShooter, Game.Deff_Shooters);
}

function CreateBonus(){
    var Type = Math.random() < 0.5 ? ObjectType.Sheild : ObjectType.Coin
    var Tmp = new Model(Type)
    Tmp.live(RandVal(45), 0, -20)
    Tmp.AutoMoveDown()
    if(Player != null || Player != undefined) setTimeout(setTimeout(CreateBonus, 6000));
}

function StartGame() {
    Player = new Model(ObjectType.Player, 3, 1)
    Player.live(0, 0, 20)

    CreateKamikazes()
    setTimeout(CreateBonus, 6000)
    setTimeout(CreateShooter, Game.Deff_Shooters)

}

function EndGame(){
    Enemies.kamikazes.forEach(elm => {
        elm.die()
    })
    Enemies.Shooters.forEach(elm => {
        elm.die()
    })

    Enemies.kamikazes = []
    Enemies.Shooters = []

    // Game.Score = 0 ; Game.Deff_Shooters = 5000 ; Game.Deff_kamikazes = 2000 ;

    Player = null
    
    while(scene.children.length > 0){ 
        scene.remove(scene.children[0]); 
    }
}